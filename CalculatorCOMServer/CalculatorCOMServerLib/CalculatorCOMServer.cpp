// CalculatorCOMServer.cpp : Implementation of WinMain


#include "pch.h"
#include "framework.h"
#include "resource.h"
#import "CalculatorCOMServer.tlb"


using namespace ATL;
using namespace CalculatorCOMServerLib;

class CCalculatorCOMServerModule : public ATL::CAtlExeModuleT< CCalculatorCOMServerModule >
{
public :
	DECLARE_LIBID(__uuidof(__CalculatorCOMServerLib));
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_CALCULATORCOMSERVER, "{3d5c6479-c146-485d-a27e-b8d263a28abc}")
};

CCalculatorCOMServerModule _AtlModule;



//
extern "C" int WINAPI _tWinMain(HINSTANCE /*hInstance*/, HINSTANCE /*hPrevInstance*/,
								LPTSTR /*lpCmdLine*/, int nShowCmd)
{
	return _AtlModule.WinMain(nShowCmd);
}

