// dllmain.h : Declaration of module class.
#import "CalculatorCOMv2.tlb"

class CCalculatorCOMv2Module : public ATL::CAtlDllModuleT< CCalculatorCOMv2Module >
{
public :
	DECLARE_LIBID(__uuidof(CalculatorCOMv2Lib::__CalculatorCOMv2Lib))
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_CALCULATORCOMV2, "{2634f263-cc7c-4a38-a93a-7a632f2c227f}")
};

extern class CCalculatorCOMv2Module _AtlModule;
