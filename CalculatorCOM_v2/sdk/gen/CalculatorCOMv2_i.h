

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 8.00.0603 */
/* at Mon Aug 30 09:21:40 2021
 */
/* Compiler settings for shared\include\CalculatorCOMv2.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 8.00.0603 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 500
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __CalculatorCOMv2_i_h__
#define __CalculatorCOMv2_i_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __ICalculatorObj_FWD_DEFINED__
#define __ICalculatorObj_FWD_DEFINED__
typedef interface ICalculatorObj ICalculatorObj;

#endif 	/* __ICalculatorObj_FWD_DEFINED__ */


#ifndef __CalculatorObj_FWD_DEFINED__
#define __CalculatorObj_FWD_DEFINED__

#ifdef __cplusplus
typedef class CalculatorObj CalculatorObj;
#else
typedef struct CalculatorObj CalculatorObj;
#endif /* __cplusplus */

#endif 	/* __CalculatorObj_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"
#include "shobjidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __ICalculatorObj_INTERFACE_DEFINED__
#define __ICalculatorObj_INTERFACE_DEFINED__

/* interface ICalculatorObj */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ICalculatorObj;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("1d1c70ee-f413-47f5-ae83-e9f7d857dc53")
    ICalculatorObj : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Sum( 
            /* [in] */ LONGLONG iLeft,
            /* [in] */ LONGLONG iRight,
            /* [retval][out] */ LONGLONG *iRes) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Subtract( 
            /* [in] */ LONGLONG iLeft,
            /* [in] */ LONGLONG iRight,
            /* [retval][out] */ LONGLONG *iRes) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Multiply( 
            /* [in] */ LONGLONG iLeft,
            /* [in] */ LONGLONG iRight,
            /* [retval][out] */ LONGLONG *iRes) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Divide( 
            /* [in] */ LONGLONG iLeft,
            /* [in] */ LONGLONG iRight,
            /* [retval][out] */ LONGLONG *iRes) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct ICalculatorObjVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ICalculatorObj * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ICalculatorObj * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ICalculatorObj * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ICalculatorObj * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ICalculatorObj * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ICalculatorObj * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ICalculatorObj * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Sum )( 
            ICalculatorObj * This,
            /* [in] */ LONGLONG iLeft,
            /* [in] */ LONGLONG iRight,
            /* [retval][out] */ LONGLONG *iRes);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Subtract )( 
            ICalculatorObj * This,
            /* [in] */ LONGLONG iLeft,
            /* [in] */ LONGLONG iRight,
            /* [retval][out] */ LONGLONG *iRes);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Multiply )( 
            ICalculatorObj * This,
            /* [in] */ LONGLONG iLeft,
            /* [in] */ LONGLONG iRight,
            /* [retval][out] */ LONGLONG *iRes);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Divide )( 
            ICalculatorObj * This,
            /* [in] */ LONGLONG iLeft,
            /* [in] */ LONGLONG iRight,
            /* [retval][out] */ LONGLONG *iRes);
        
        END_INTERFACE
    } ICalculatorObjVtbl;

    interface ICalculatorObj
    {
        CONST_VTBL struct ICalculatorObjVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ICalculatorObj_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ICalculatorObj_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ICalculatorObj_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ICalculatorObj_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ICalculatorObj_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ICalculatorObj_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ICalculatorObj_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ICalculatorObj_Sum(This,iLeft,iRight,iRes)	\
    ( (This)->lpVtbl -> Sum(This,iLeft,iRight,iRes) ) 

#define ICalculatorObj_Subtract(This,iLeft,iRight,iRes)	\
    ( (This)->lpVtbl -> Subtract(This,iLeft,iRight,iRes) ) 

#define ICalculatorObj_Multiply(This,iLeft,iRight,iRes)	\
    ( (This)->lpVtbl -> Multiply(This,iLeft,iRight,iRes) ) 

#define ICalculatorObj_Divide(This,iLeft,iRight,iRes)	\
    ( (This)->lpVtbl -> Divide(This,iLeft,iRight,iRes) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ICalculatorObj_INTERFACE_DEFINED__ */



#ifndef __CalculatorCOMv2Lib_LIBRARY_DEFINED__
#define __CalculatorCOMv2Lib_LIBRARY_DEFINED__

/* library CalculatorCOMv2Lib */
/* [version][uuid] */ 


EXTERN_C const IID LIBID_CalculatorCOMv2Lib;

EXTERN_C const CLSID CLSID_CalculatorObj;

#ifdef __cplusplus

class DECLSPEC_UUID("17c4a716-2c78-4015-b187-7fa0de362933")
CalculatorObj;
#endif
#endif /* __CalculatorCOMv2Lib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


