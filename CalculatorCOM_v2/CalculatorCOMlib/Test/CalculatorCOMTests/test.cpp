#include "pch.h"
#include "CalculatorCOMv2_i.h"
#include "CalculatorCOMv2_i.c"
#include <atlbase.h>

TEST(TestCOMServer, test_all_operations) {
	CoInitialize(NULL);
	CComQIPtr<ICalculatorObj> pCalc;
	auto hr = pCalc.CoCreateInstance(CLSID_CalculatorObj);
	ASSERT_NE(pCalc.p, nullptr);
	LONGLONG iRes = 0;
	pCalc->Sum(10, 15, &iRes);
	EXPECT_EQ(iRes, 25);
	pCalc->Subtract(10, 15, &iRes);
	EXPECT_EQ(iRes, -5);
	pCalc->Multiply(10, 15, &iRes);
	EXPECT_EQ(iRes, 150);
	pCalc->Divide(10, 2, &iRes);
	EXPECT_EQ(iRes, 5);
}