// CalculatorObj.cpp : Implementation of CCalculatorObj

#include "pch.h"
#include "CalculatorObj.h"


// CCalculatorObj

STDMETHODIMP CCalculatorObj::Sum(LONGLONG iLeft, LONGLONG iRight, LONGLONG* iRes)
{
	*iRes = iLeft + iRight;
	return S_OK;
}
STDMETHODIMP CCalculatorObj::Subtract(LONGLONG iLeft, LONGLONG iRight, LONGLONG* iRes)
{
	*iRes = iLeft - iRight;
	return S_OK;
}
STDMETHODIMP CCalculatorObj::Multiply(LONGLONG iLeft, LONGLONG iRight, LONGLONG* iRes)
{
	*iRes = iLeft * iRight;
	return S_OK;
}
STDMETHODIMP CCalculatorObj::Divide(LONGLONG iLeft, LONGLONG iRight, LONGLONG* iRes)
{
	*iRes = iLeft / iRight;
	return S_OK;
}
