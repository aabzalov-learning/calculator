#include "pch.h"
#include "CalculatorObj.h"
// CalculatorObj.cpp : Implementation of CCalculatorObj

STDMETHODIMP CCalculatorObj::Sum(LONGLONG iLeft, LONGLONG iRight, LONGLONG* iRes)
{
	// TODO: Add your implementation code here
	*iRes = iLeft + iRight;
	return S_OK;
}

STDMETHODIMP CCalculatorObj::Subtract(LONGLONG iLeft, LONGLONG iRight, LONGLONG* iRes)
{
	// TODO: Add your implementation code here
	*iRes = iLeft - iRight;
	return S_OK;
}

STDMETHODIMP CCalculatorObj::Multiply(LONGLONG iLeft, LONGLONG iRight, LONGLONG* iRes)
{
	// TODO: Add your implementation code here
	*iRes = iLeft * iRight;
	return S_OK;
}

STDMETHODIMP CCalculatorObj::Divide(LONGLONG iLeft, LONGLONG iRight, LONGLONG* iRes)
{
	// TODO: Add your implementation code here
	*iRes = iLeft / iRight;
	return S_OK;
}

