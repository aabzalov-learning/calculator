#include "pch.h"
#import "CalculatorCOMLib.tlb" raw_interfaces_only
#include <atlsafe.h>
#include <atlbase.h>

using namespace ATL;
using namespace CalculatorCOMLib;

TEST(TestOutOfProcServer, test_calculation_operations) {
	CoInitialize(NULL);
	CComQIPtr<ICalculatorObj> pCalc;
	auto hr = pCalc.CoCreateInstance(__uuidof(CalculatorObj));
	ASSERT_NE(pCalc.p, nullptr);
	LONGLONG iRes = 0;
	pCalc->Sum(10, 15, &iRes);
	EXPECT_EQ(iRes, 25);
	pCalc->Subtract(10, 15, &iRes);
	EXPECT_EQ(iRes, -5);
	pCalc->Multiply(10, 15, &iRes);
	EXPECT_EQ(iRes, 150);
	pCalc->Divide(10, 2, &iRes);
	EXPECT_EQ(iRes, 5);
}