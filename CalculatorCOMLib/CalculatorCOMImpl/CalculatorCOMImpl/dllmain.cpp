// dllmain.cpp : Implementation of DllMain.

#include "pch.h"
#include "framework.h"
#include "resource.h"
//#include "CalculatorCOMImpl_i.h"
#import "CalculatorCOMLib.tlb" raw_interfaces_only
#include "dllmain.h"

CCalculatorCOMImplModule _AtlModule;

// DLL Entry Point
extern "C" BOOL WINAPI DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	hInstance;
	return _AtlModule.DllMain(dwReason, lpReserved);
}
