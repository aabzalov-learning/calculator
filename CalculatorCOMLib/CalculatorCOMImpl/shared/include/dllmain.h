// dllmain.h : Declaration of module class.
#import "CalculatorCOMLib.tlb" raw_interfaces_only

class CCalculatorCOMImplModule : public ATL::CAtlDllModuleT< CCalculatorCOMImplModule >
{
public :
	//DECLARE_LIBID(LIBID_CalculatorCOMLib)
	DECLARE_LIBID(__uuidof(CalculatorCOMLib::__CalculatorCOMLib))
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_CALCULATORCOMIMPL, "{5807e4a7-eae4-4e07-9b4d-54459050f6c3}")
};

extern class CCalculatorCOMImplModule _AtlModule;
