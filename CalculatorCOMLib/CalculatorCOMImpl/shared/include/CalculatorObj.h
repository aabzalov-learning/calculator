#pragma once
#include "resource.h"       // main symbols
#include "pch.h"
#import "CalculatorCOMLib.tlb" raw_interfaces_only

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif

using namespace ATL;
using namespace CalculatorCOMLib;

// CCalculatorObj

class ATL_NO_VTABLE CCalculatorObj :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CCalculatorObj, &__uuidof(CalculatorObj)>,
	public IDispatchImpl<ICalculatorObj, &__uuidof(ICalculatorObj), &__uuidof(__CalculatorCOMLib), /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CCalculatorObj()
	{
	}

DECLARE_REGISTRY_RESOURCEID(106)


BEGIN_COM_MAP(CCalculatorObj)
	COM_INTERFACE_ENTRY(ICalculatorObj)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()



	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

	STDMETHOD(Sum)(LONGLONG iLeft, LONGLONG iRight, LONGLONG* iRes);
	STDMETHOD(Subtract)(LONGLONG iLeft, LONGLONG iRight, LONGLONG* iRes);
	STDMETHOD(Multiply)(LONGLONG iLeft, LONGLONG iRight, LONGLONG* iRes);
	STDMETHOD(Divide)(LONGLONG iLeft, LONGLONG iRight, LONGLONG* iRes);
};

OBJECT_ENTRY_AUTO(__uuidof(CalculatorObj), CCalculatorObj)
