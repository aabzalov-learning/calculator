#include "IAction.h"

int64_t CSumAction::DoOperation(int64_t iLeft, int64_t iRight) const 
{
	return iLeft + iRight;
};

int64_t CSubtractAction::DoOperation(int64_t iLeft, int64_t iRight) const 
{
	return iLeft - iRight;
};

int64_t CDivideAction::DoOperation(int64_t iLeft, int64_t iRight) const 
{
	return iLeft / iRight;
};

int64_t CMultiplyAction::DoOperation(int64_t iLeft, int64_t iRight) const 
{
	return iLeft * iRight;
};

