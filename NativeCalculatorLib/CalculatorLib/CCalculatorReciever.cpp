#include "CCalculatorReciever.h"

void CCalculatorReciever::RefreshResults(uint64_t iIndex, int64_t iResult)
{
	m_iIndex = iIndex;
	m_iResult = iResult;
}

CCalculatorReciever::CCalculatorReciever(CCalculator& Calculator) noexcept:
	m_Calculator(Calculator)
{
	m_Connection = m_Calculator.Connect(
		std::bind(&CCalculatorReciever::RefreshResults, this, 
			std::placeholders::_1, std::placeholders::_2)
	);
}

std::tuple<uint64_t, int64_t> CCalculatorReciever::GetIndexAndResult()
{
	return std::make_tuple(m_iIndex, m_iResult);
}

CCalculatorReciever::~CCalculatorReciever() noexcept
{
	m_Connection.disconnect();
}
