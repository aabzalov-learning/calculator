#include "CCalculator.h"
std::unique_ptr<ICommand> CCalculator::GetSpecificCommand(char chOperator, int64_t iLeft, int64_t iRight)
{
	switch (chOperator)
	{
	case('+'):
		return std::make_unique<CCalculateSumCommand>(iLeft, iRight);
	case('-'):
		return std::make_unique<CCalculateSubtractCommand>(iLeft, iRight);
	case('*'):
		return std::make_unique<CCalculateMultiplyCommand>(iLeft, iRight);
	case('/'):
		return std::make_unique<CCalculateDivideCommand>(iLeft, iRight);
	default:
		return std::make_unique<CEmptyCommand>();
	}
}

int64_t CCalculator::GetResultCacheValue() const
{
	return m_iResultCache;
}

uint64_t CCalculator::GetOperationCounterValue() const
{
	return m_cOperation;
}

int64_t CCalculator::ExecuteOperation(char chOperator, int64_t iLeft, int64_t iRight)
{
	++m_cOperation;
	if (iRight == g_i64Max) 
	{
		iRight = iLeft;
		iLeft = m_iResultCache;
	}
	auto pCurCommand = GetSpecificCommand(chOperator, iLeft, iRight);
	m_iResultCache = pCurCommand->Execute();
	m_Signal(m_cOperation, m_iResultCache);
	return m_iResultCache;
}

boost::signals2::connection CCalculator::Connect(const signal_type::slot_type& slotSubscriber)
{
	return m_Signal.connect(slotSubscriber);
}


