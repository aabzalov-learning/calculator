#include "pch.h"
#include "CCalculator.h"
#include "CCalculatorReciever.h"
#include "gtest/gtest.h"
TEST(CalculatorTest, test_operators_with_two_operands) 
{
	auto CalculatorInstance = CCalculator{};
	EXPECT_EQ(10, CalculatorInstance.ExecuteOperation('+', 5, 5));
	EXPECT_EQ(0, CalculatorInstance.ExecuteOperation('-', 5, 5));
	EXPECT_EQ(25, CalculatorInstance.ExecuteOperation('*', 5, 5));
	EXPECT_EQ(5, CalculatorInstance.ExecuteOperation('/', 25, 5));
	EXPECT_EQ(10, CalculatorInstance.ExecuteOperation('+', 5));
}

TEST(CalculatorTest, test_operators_with_one_operand)
{
	auto CalculatorInstance = CCalculator{};
	EXPECT_EQ(2, CalculatorInstance.ExecuteOperation('+', 2));
	EXPECT_EQ(-3, CalculatorInstance.ExecuteOperation('-', 5));
	EXPECT_EQ(-15, CalculatorInstance.ExecuteOperation('*', 5));
	EXPECT_EQ(-3, CalculatorInstance.ExecuteOperation('/', 5));
}

TEST(CalculatorTest, test_invalid_operator)
{
	auto CalculatorInstance = CCalculator{};
	EXPECT_EQ(g_i64Max, CalculatorInstance.ExecuteOperation('F', 2, 2));
}

TEST(CalculatorTest, test_signals)
{
	auto CalculatorInstance = CCalculator{};
	auto CalculatorReciever = CCalculatorReciever{ CalculatorInstance };
	CalculatorInstance.ExecuteOperation('+', 5, 10);
	auto [iIndex, iResult] = CalculatorReciever.GetIndexAndResult();
	EXPECT_EQ(1, iIndex);
	EXPECT_EQ(15, iResult);
}
