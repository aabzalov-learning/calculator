#include "ICommand.h"

int64_t CCalculateCommand::CaclulationProcess(const int64_t iLeft, const int64_t iRight) const
{
	return m_pAction->DoOperation(m_iLeft, m_iRight);

};

void CCalculateCommand::CommandResultLog(const int64_t iResult) const
{
	std::cout << "Current action: " << GetCommandName() << ", result: " << iResult << '\n';
};

CCalculateCommand::CCalculateCommand(std::unique_ptr<IAction> pAction, int64_t iLeft, int64_t iRight) noexcept :
	m_pAction(std::move(pAction)), m_iLeft(iLeft), m_iRight(iRight)
{};

int64_t CCalculateCommand::Execute() const
{
	const auto iResult = CaclulationProcess(m_iLeft, m_iRight);
	CommandResultLog(iResult);
	return iResult;
};

const std::string CCalculateSumCommand::GetCommandName() const
{
	return "Sum";
};

CCalculateSumCommand::CCalculateSumCommand(int64_t iLeft, int64_t iRight) noexcept :
	CCalculateCommand(std::make_unique<CSumAction>(), iLeft, iRight)
{};

const std::string CCalculateSubtractCommand::GetCommandName() const
{
	return "Subtract";
};

CCalculateSubtractCommand::CCalculateSubtractCommand(int64_t iLeft, int64_t iRight) noexcept :
	CCalculateCommand(std::make_unique<CSubtractAction>(), iLeft, iRight)
{};

const std::string CCalculateDivideCommand::GetCommandName() const
{
	return "Divide";
};

CCalculateDivideCommand::CCalculateDivideCommand(int64_t iLeft, int64_t iRight) noexcept :
	CCalculateCommand(std::make_unique<CDivideAction>(), iLeft, iRight)
{};

const std::string CCalculateMultiplyCommand::GetCommandName() const
{
	return "Multiply";
};

CCalculateMultiplyCommand::CCalculateMultiplyCommand(int64_t iLeft, int64_t iRight) noexcept :
	CCalculateCommand(std::make_unique<CMultiplyAction>(), iLeft, iRight)
{};

const std::string CEmptyCommand::GetCommandName() const
{
	return "Empty command";
};

void CEmptyCommand::CommandResultLog(int64_t iResult) const
{
	std::cout << "Action: " << GetCommandName() << '\n';
};

int64_t CEmptyCommand::Execute() const
{
	CommandResultLog(0);
	return g_i64Max;
};
