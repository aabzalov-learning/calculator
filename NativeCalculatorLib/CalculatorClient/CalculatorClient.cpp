#include "CCalculator.h"
#include <boost/program_options.hpp>
#include <map>

namespace po = boost::program_options;

int main(int ac, char* av[])
{
    std::string CurAction;
    int64_t iLeft = 0, iRight = 0;
    try {

        po::options_description desc("Allowed options");
        desc.add_options()
            ("help", "produce help message")
            ("action,a", po::value<std::string>(&CurAction), "set action")
            ("first,f", po::value<int64_t>(&iLeft), "set first parameter")
            ("second,s", po::value<int64_t>(&iRight)->default_value(g_i64Max), "set second parameter")
        ;

        po::variables_map vm;        
        po::store(po::parse_command_line(ac, av, desc), vm);
        po::notify(vm);    

        if (vm.count("help")) 
        {
            std::cout << desc << "\n";
            return 0;
        }

    }
    catch(std::exception& e)
    {
        std::cerr << "error: " << e.what() << "\n";
        return 1;
    }
    catch(...)
    {
        std::cerr << "Exception of unknown type!\n";
    }

    const std::map<const std::string, char> Operations = 
    {
        {"Sum", '+'},
        {"Subtract", '-'},
        {"Multiply", '*'},
        {"Divide", '/' }
    };
    auto CalcInstance = CCalculator{};
    auto iRes = CalcInstance.ExecuteOperation(Operations.at(CurAction), iLeft, iRight);
    return static_cast<int>(iRes);
}

