#pragma once
#include "CCalculator.h"

class CCalculatorReciever
{
	CCalculator& m_Calculator;
	boost::signals2::connection m_Connection;
	uint64_t m_iIndex = 0;
	int64_t m_iResult = 0;

	void RefreshResults(uint64_t iIndex, int64_t iResult);
public:
	CCalculatorReciever(CCalculator& Calculator) noexcept;
	std::tuple<uint64_t, int64_t> GetIndexAndResult();
	~CCalculatorReciever() noexcept;
};
