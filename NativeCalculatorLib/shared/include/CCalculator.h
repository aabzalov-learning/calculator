#pragma once
#include "ICommand.h"
#include "pch.h"
#include <boost/signals2.hpp>

class CCalculator 
{
	uint64_t m_cOperation = 0;
	int64_t m_iResultCache = 0;
	using signal_type = boost::signals2::signal<void (uint64_t, int64_t)>;
	signal_type m_Signal;

	std::unique_ptr<ICommand> GetSpecificCommand(char chOperator, int64_t iLeft, int64_t iRight);
public:
	int64_t GetResultCacheValue() const;
	uint64_t GetOperationCounterValue() const;
	int64_t ExecuteOperation(char chOperator, int64_t iLeft, int64_t iRight = g_i64Max);
	boost::signals2::connection Connect(const signal_type::slot_type& slotSubscriber);

};

