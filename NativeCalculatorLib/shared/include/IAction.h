#pragma once
#include "pch.h"

interface IAction 
{
	virtual int64_t DoOperation(int64_t iLeft, int64_t iRight) const = 0;
};

struct CSumAction : public IAction 
{
	virtual int64_t DoOperation(int64_t iLeft, int64_t iRight) const override;
};

struct CSubtractAction : public IAction 
{
	virtual int64_t DoOperation(int64_t iLeft, int64_t iRight) const override;
};


struct CMultiplyAction : public IAction 
{
	virtual int64_t DoOperation(int64_t iLeft, int64_t iRight) const override;
};

struct CDivideAction : public IAction 
{
	virtual int64_t DoOperation(int64_t iLeft, int64_t iRight) const override;
};
