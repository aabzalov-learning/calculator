#pragma once
#include "IAction.h"
#include "pch.h"

interface ICommand 
{
	virtual int64_t Execute() const = 0;
private:
	virtual void CommandResultLog(const int64_t iResult) const = 0;
protected:
	virtual const std::string GetCommandName() const = 0;
};

class CCalculateCommand : public ICommand 
{
	std::unique_ptr<IAction> m_pAction = nullptr;
	int64_t m_iLeft = 0, m_iRight = 0;

	virtual int64_t CaclulationProcess(const int64_t iLeft, const int64_t iRight) const;
	virtual void CommandResultLog(const int64_t iResult) const override;
public:
	explicit CCalculateCommand(std::unique_ptr<IAction> pAction, int64_t iLeft, int64_t iRight) noexcept;
	int64_t Execute() const override;
};

class CCalculateSumCommand : public CCalculateCommand 
{
	virtual const std::string GetCommandName() const override;
public:
	explicit CCalculateSumCommand(int64_t iLeft, int64_t iRight) noexcept;
};

class CCalculateSubtractCommand : public CCalculateCommand 
{  
	virtual const std::string GetCommandName() const override;
public:
	explicit CCalculateSubtractCommand(int64_t iLeft, int64_t iRight) noexcept;
};

class CCalculateDivideCommand : public CCalculateCommand 
{
	virtual const std::string GetCommandName() const override;
public:
	explicit CCalculateDivideCommand(int64_t iLeft, int64_t iRight) noexcept;
};

class CCalculateMultiplyCommand : public CCalculateCommand 
{
	virtual const std::string GetCommandName() const override;
public:
	explicit CCalculateMultiplyCommand(int64_t iLeft, int64_t iRight) noexcept;
};

class CEmptyCommand : public ICommand 
{
	virtual const std::string GetCommandName() const override;
	virtual void CommandResultLog(int64_t iResult) const override;
public:
	virtual int64_t Execute() const override;
};
